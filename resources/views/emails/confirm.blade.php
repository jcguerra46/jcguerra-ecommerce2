@component('mail::message')
# Hola {{ $user->name }}!

Haz cambiado tu correo electrónico. Por favor confirma tu nuevo correo usando el siguiente botón:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Confirmar mi cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent