<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, softDeletes;

    const USUARIO_VERIFICADO = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const USUARIO_ADMINISTRADOR = 'true';
    const USUARIO_REGULAR = 'false';

    protected $table = 'users';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'verified',
        'verification_token',
        'admin',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        'verification_token',
    ];

        // Mutator
        public function setNameAttribute($valor)
        {
            return $this->attributes['name'] = strtolower($valor);
        }

        public function getNameAttribute($valor)
        {
            return ucwords($valor);
        }

        public function setEmailAttribute($valor)
        {
            return $this->attributes['email'] = strtolower($valor);
        }

        // fin mutator

    public function esVerificado()
    {
        return $this->verified == user::USUARIO_VERIFICADO;
    }

    public function esAdministrador()
    {
        return $this->admin == user::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificationToken()
    {
        return str_random(40);
    }
}