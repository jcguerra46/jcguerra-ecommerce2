<?php

namespace App\Http\Middleware;

use Closure;

class SignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next, $header = 'X-Name')
    {
        // Before Middleware

        // -------------------------------
        $response = $next($request);

        // After Middleware
        $response->headers->set($header, config('app.name'));

        // -------------------------------
        return $response;
    }
}
