<?php

namespace App\Http\Controllers\Seller;

use App\Seller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class SellerTransactionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        $transacions = $seller->products()
            ->whereHas('transacions')
            ->with('transacions')
            ->get()
            ->pluck('transacions')
            ->collapse();

        return $this->showAll($transacions);
    }
}
